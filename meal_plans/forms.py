from django.forms import ModelForm
from meal_plans.models import MealPlan

# mealPlanForm
class MealPlanForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = ["name", "recipes", "date"]


# mealPlanDeleteForm
class MealPlanDeleteForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = []
